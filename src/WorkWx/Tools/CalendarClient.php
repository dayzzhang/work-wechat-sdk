<?php


namespace saber\WorkWechat\WorkWx\Tools;


use saber\WorkWechat\Core\HttpCent;

/**日历
 * Class CalendarClient
 * @package saber\WorkWechat\WorkWx\Tools
 * @link https://developer.work.weixin.qq.com/document/path/93647
 */
class CalendarClient extends HttpCent
{
    /**添加日历
     * @param int $agentId
     * @param array $calendar
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function add( $agentId, $calendar )
    {
        return $this->httpPostJson('/cgi-bin/oa/calendar/add', ['agentid' => $agentId, 'calendar' => $calendar]);
    }

    /**更新日历
     * @param $calendar
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update($calendar){
        return $this->httpPostJson('/cgi-bin/oa/calendar/update', ['calendar' => $calendar]);
    }

    /**获取日历详情
     * @param array|string $calendarIds
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get( $calendarIds )
    {
        if( is_string($calendarIds) ){
            $calendarIds = [$calendarIds];
        }
        return $this->httpPostJson('/cgi-bin/oa/calendar/get', ['cal_id_list' => $calendarIds]);
    }

    /**删除日历
     * @param string $calId
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function del( $calId )
    {
        return $this->httpPostJson('/cgi-bin/oa/calendar/del', ['cal_id' => $calId]);
    }
}