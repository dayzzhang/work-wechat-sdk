<?php


namespace saber\WorkWechat\WorkWx\Tools;


use saber\WorkWechat\Core\HttpCent;

/**日程
 * Class ScheduleClient
 * @package saber\WorkWechat\WorkWx\Tools
 * @link https://developer.work.weixin.qq.com/document/path/93648
 */
class ScheduleClient extends HttpCent
{

    /**创建日程
     * @param int $agentId
     * @param array $schedule
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function add( $agentId, $schedule )
    {
        return $this->httpPostJson('/cgi-bin/oa/schedule/add', ['agentid' => $agentId, 'schedule' => $schedule]);
    }

    /**更新日程
     * @param $schedule
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update( $schedule )
    {
        return $this->httpPostJson('/cgi-bin/oa/schedule/update', ['schedule' => $schedule]);
    }

    /**获取日程详情
     * @param array|string $scheduleIds
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get( $scheduleIds )
    {
        if( is_string($scheduleIds) ){
            $scheduleIds = [$scheduleIds];
        }
        return $this->httpPostJson('/cgi-bin/oa/schedule/get', ['schedule' => $scheduleIds]);
    }

    /**取消日程
     * @param string $scheduleId
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function del( $scheduleId ){
        return $this->httpPostJson('/cgi-bin/oa/schedule/get',  ['schedule_id'=>$scheduleId]);
    }


    /**获取日历下的日程列表
     * @param string $calId 日历ID
     * @param null|int $offset
     * @param null|int $limit
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getByCalendar( $calId, $offset=null, $limit=null )
    {
        return $this->httpPostJson('/cgi-bin/oa/schedule/get_by_calendar', ['cal_id' => $calId, 'offset' => $offset, 'limit' => $limit]);
    }

    /**新增日程参与者
     * @param string $scheduleId
     * @param array $attendees
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addAttendees( $scheduleId, $attendees )
    {
        return $this->httpPostJson('/cgi-bin/oa/schedule/get_by_calendar', ['schedule_id' => $scheduleId, 'attendees' => $attendees]);
    }

    /**删除日程参与者
     * @param $scheduleId
     * @param $attendees
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function delAttendees( $scheduleId, $attendees ){
        return $this->httpPostJson('/cgi-bin/oa/schedule/get_by_calendar', ['schedule_id' => $scheduleId, 'attendees' => $attendees]);
    }


}