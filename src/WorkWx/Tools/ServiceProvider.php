<?php


namespace saber\WorkWechat\WorkWx\Tools;


use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{

    public function register( Container $pimple )
    {
        $pimple['calendar'] = function($app){
            return new CalendarClient($app);
        };
        $pimple['schedule'] = function($app){
            return new ScheduleClient($app);
        };
    }
}