<?php


namespace saber\WorkWechat\WorkWx\Service;


use saber\WorkWechat\Core\HttpCent;

class Client extends HttpCent
{

    /**获取第三方应用凭证
     * @param string $suiteId 以ww或wx开头应用id（对应于旧的以tj开头的套件id）
     * @param string $suiteSecret 应用secret
     * @param string $suiteTicket 企业微信后台推送的ticket
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getSuiteToken($suiteId, $suiteSecret, $suiteTicket)
    {
        $param = [
            'suite_id' => $suiteId,
            'suite_secret' => $suiteSecret,
            'suite_ticket' => $suiteTicket
        ];
        return $this->httpPostJson('/cgi-bin/service/get_suite_token', $param);
    }

    /**获取预授权码
     * @param string $suiteAccessToken
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getPreAuthCode($suiteAccessToken)
    {
        return $this->httpGet('/cgi-bin/service/get_suite_token', [ 'suite_access_token' => $suiteAccessToken ]);
    }

    /**设置授权配置
     * @param string $preAuthCode
     * @param array $appId
     * @param string $authType
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function setSessionInfo($preAuthCode, $appId, $authType)
    {
        $param = [
            'pre_auth_code' => $preAuthCode,
            'session_info' => [
                'appid' => $appId,
                'auth_type' => $authType,
            ]
        ];
        return $this->httpPostJson('/cgi-bin/service/set_session_info', $param);
    }


    /**获取企业永久授权码
     * @param string $authCode
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getPermanentCode($authCode){
        return $this->httpPostJson('/cgi-bin/service/get_permanent_code', ['auth_code'=>$authCode]);
    }

    /**获取企业授权信息
     * @param string $authCorpId
     * @param string $permanentCode
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAuthInfo($authCorpId,$permanentCode){
        $param = [
            'auth_corpid' => $authCorpId,
            'permanent_code' => $permanentCode
        ];
        return $this->httpPostJson('/cgi-bin/service/get_permanent_code', $param);
    }


    /**获取企业凭证
     * @param string $authCorpId
     * @param string $permanentCode
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @link  https://work.weixin.qq.com/api/doc/90001/90143/90605
     */
    public function getCorpToken($authCorpId,$permanentCode){
        $param = [
            'auth_corpid' => $authCorpId,
            'permanent_code' => $permanentCode
        ];
        return $this->httpPostJson('/cgi-bin/service/get_corp_token', $param);
    }

    /**获取应用的管理员列表
     * @param string $authCorpId
     * @param int $agentId
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAdminList($authCorpId,$agentId){
        $param = [
            'auth_corpid' => $authCorpId,
            'agentid' => $agentId
        ];
        return $this->httpPostJson('/cgi-bin/service/get_admin_list', $param);
    }






}