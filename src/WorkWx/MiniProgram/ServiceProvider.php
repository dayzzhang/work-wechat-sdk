<?php


namespace saber\WorkWechat\WorkWx\MiniProgram;


use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider  implements ServiceProviderInterface
{

    public function register(Container $pimple)
    {
        $pimple['MiniProgram'] = function ($app) {
            return new Client($app);
        };
    }
}