<?php


namespace saber\WorkWechat\WorkWx\MiniProgram;


use saber\WorkWechat\Core\HttpCent;

class Client extends HttpCent
{
    /**发送应用消息
     * @param array $param
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @see https://work.weixin.qq.com/api/doc/90001/90143/90372
     */
    public function session( $code ){
        return $this->httpGet('/cgi-bin/miniprogram/jscode2session', ['js_code'=>$code,'grant_type'=>'authorization_code']);
    }
}