<?php


namespace saber\WorkWechat\WorkWx\Media;


use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{

    public function register(Container $pimple)
    {
        $pimple['media'] = function ($app) {
            return new Client($app);
        };
    }
}