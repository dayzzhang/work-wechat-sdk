<?php


namespace saber\WorkWechat\WorkWx\Media;


use saber\WorkWechat\Core\HttpCent;

class Client extends HttpCent
{
    /**上传临时文件
     * @param string $type
     * @param $file
     * @param null|string $fileName
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \saber\WorkWechat\Core\Exceptions\AccessTokenNotFindExceptions
     * @throws \saber\WorkWechat\Core\Exceptions\NotInstanceofExceptions
     */
    public function upload($type, $file, $fileName = null)
    {
        $multipart = [
            [
                'name' => 'filename',
                'contents' => $file,
                'filename' => empty($fileName) ? date('YmdHis').'_'.md5(time()) : $fileName
            ]
        ];
        return $this->httpPostFile('/cgi-bin/media/upload', $multipart, [ 'type' => $type ]);
    }

    /**上传图片
     * @param $file
     * @param null $fileName
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \saber\WorkWechat\Core\Exceptions\AccessTokenNotFindExceptions
     * @throws \saber\WorkWechat\Core\Exceptions\NotInstanceofExceptions
     */
    public function uploadImage($file,$fileName = null){
        $multipart = [
            [
                'name' => 'filename',
                'contents' => $file,
                'filename' => empty($fileName) ? date('YmdHis').'_'.md5(time()) : $fileName
            ]
        ];
        return $this->httpPostFile('/cgi-bin/media/uploadimg', $multipart);
    }

    /**获取素材
     * @param string $media_id
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get($media_id){
        return $this->httpGet('/cgi-bin/media/get', ['media_id'=>$media_id]);
    }

    /**获取高清素材
     * @param string $media_id
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getHD($media_id){

        return $this->httpGet('cgi-bin/media/get/jssdk', ['media_id'=>$media_id]);
    }



}