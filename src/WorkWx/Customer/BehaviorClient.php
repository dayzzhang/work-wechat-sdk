<?php


namespace saber\WorkWechat\WorkWx\Customer;


use saber\WorkWechat\Core\HttpCent;

/**
 * 用户行为统计
 * Class BehaviorClient
 * @package saber\WorkWechat\WorkWx\Customer
 */
class BehaviorClient extends HttpCent
{

    /**
     * @param int $start_time
     * @param int $end_time
     * @param array $userid
     * @param array $partyid
     */
    public function getUserBehaviorData(int $start_time,int $end_time,array $userid =[],array $partyid=[])
    {
        $data =[
            'userid'  => $userid,
            'partyid' => $partyid,
            'start_time'=>$start_time,
            'end_time'  =>$end_time
        ];

        return $this->httpPostJson('/cgi-bin/externalcontact/get_user_behavior_data',$data);
    }

    /**
     * @param int $begin_time
     * @param int $end_time
     * @param array $userList
     * @param int $order_by 1 - 新增群的数量 2 - 群总数3 - 新增群人数 4 - 群总人数
     * @param int $order_asc 0-降序；1-升序
     * @param int $offset  分页，偏移量, 默认为0
     * @param int $limit 分页，预期请求的数据量，默认为500，取值范围 1 ~ 1000
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getGroupLeaderBehaviorData(int $begin_time,int $end_time,array $userList =[],int $order_by=1,int $order_asc=0,int $offset=0, int $limit =500)
    {
        $data = [
            'day_begin_time' => $begin_time,
            'day_end_time' => $end_time,
            'owner_filter' => [
                'userid_list' => $userList
            ],
            'order_by' => $order_by,
            'order_asc' => $order_asc,
            'offset' => $offset,
            'limit' => $limit
        ];

        return $this->httpPostJson('/cgi-bin/externalcontact/groupchat/statistic',$data);
    }

    /**
     * @param $begin_time
     * @param int|null $end_time
     * @param array $userList
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getDayBehaviorData(int $begin_time,int $end_time = null,$userList=[]){
        $data = [
            'day_begin_time' => $begin_time,
            'day_end_time' => $end_time,
            'owner_filter' => [
                'userid_list' => $userList
            ],
        ];

        return $this->httpPostJson('/cgi-bin/externalcontact/groupchat/statistic_group_by_day',$data);
    }


}