<?php


namespace saber\WorkWechat\WorkWx\LinkedCorp;


use saber\WorkWechat\Core\HttpCent;

class UpDownClient  extends HttpCent
{
    /***获取下级/下游企业的access_token
     * @param string $corpId
     * @param int $agentId
     * @param null|int $businessType  填0则为企业互联/局校互联，填1则表示上下游企业
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @link https://developer.work.weixin.qq.com/document/path/95311
     */
    public function accessToken( $corpId, $agentId, $businessType = null )
    {
        return $this->httpPostJson('/cgi-bin/corpgroup/corp/gettoken', ['corpid' => $corpId, 'agentid' => $agentId, 'business_type' => $businessType]);
    }

    /**获取下级/下游企业小程序session
     * @param $userId
     * @param $sessionKey
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @link https://developer.work.weixin.qq.com/document/path/95318
     */
    public function getSession( $userId, $sessionKey )
    {
        return $this->httpPostJson('/cgi-bin/miniprogram/transfer_session', ['userid' => $userId, 'session_key' => $sessionKey]);
    }


    /**获取上下游信息
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @link https://developer.work.weixin.qq.com/document/path/95315
     */
    public function getChainList()
    {
        return $this->httpGet('/cgi-bin/corpgroup/corp/get_chain_list');
    }

    /**上下游企业应用获取微信用户的external_userid
     * @param string $unionId
     * @param string $openId
     * @param null|string $corpId
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function unId2extId( $unionId, $openId, $corpId = null )
    {
        return $this->httpPostJson('/cgi-bin/corpgroup/unionid_to_external_userid', ['unionid' => $unionId, 'openid' => $openId, 'corpid' => $corpId]);
    }

    /**获取应用共享信息
     * @param int $agentId
     * @param null|string $corpId
     * @param null|int $business_type 填0则为企业互联/局校互联，填1则表示上下游企业
     * @param int $limit
     * @param null|string $cursor
     * @return array|mixed|object|\Psr\Http\Message\ResponseInterface|\saber\WorkWechat\Core\Collection|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @link https://developer.work.weixin.qq.com/document/path/93405
     */
    public function appShareInfo( $agentId, $corpId = null, $business_type=null, $limit = 0, $cursor = null )
    {
        return $this->httpPostJson('/cgi-bin/corpgroup/corp/list_app_share_info', [
            'agentid'       => $agentId,
            'business_type' => $business_type,
            'corpid'        => $corpId,
            'limit'         => $limit,
            'cursor'        => $cursor,
        ]);
    }

}